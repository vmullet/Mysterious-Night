﻿using UnityEngine;
using System.Collections;

public class RavenFly : MonoBehaviour {

    [SerializeField] Transform LeftWing;
    [SerializeField] Transform LeftCenter;
    [SerializeField] Transform RightWing;
    [SerializeField] Transform RightCenter;

    [SerializeField] string mode;

    // Use this for initialization
    void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
        
        if (LeftWing.eulerAngles.z > 50f)
        {
            mode = "down";
        }
        else if (LeftWing.eulerAngles.z < 5f)
        {
            mode = "up";
        }

        if (mode=="up")
        {
            LeftWing.transform.RotateAround(LeftCenter.position, Vector3.forward, 5f);
            RightWing.transform.RotateAround(RightCenter.position, Vector3.forward, 5f);
        }
        else
        {
            LeftWing.transform.RotateAround(LeftCenter.position, Vector3.forward, -5f);
            RightWing.transform.RotateAround(RightCenter.position, Vector3.forward, -5f);
        }
        

    }

    public void ResetWings()
    {
        LeftWing.eulerAngles = Vector3.zero;
        RightWing.eulerAngles = Vector3.zero;
    }
}
